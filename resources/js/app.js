require("./bootstrap");

import Vue from "vue";
import VueRouter from "vue-router";
import Root from "./components/Root";
import Dashboard from "./components/Dashboard";

Vue.use(VueRouter);

const routes = {
    mode: "history",

    routes: [
        {
            path: "/overview/dashboard",
            name: "dashboard",
            component: Dashboard,
        },
    ],
};

const router = new VueRouter(routes);

new Vue({
    el: "#app",
    router,
    components: { Root },
});
